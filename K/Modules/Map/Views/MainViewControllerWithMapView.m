//
//  MainViewControllerWithMapView.m
//  K
//
//  Created by OUT-Volkov-AD on 10/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import "MainViewControllerWithMapView.h"
#import "MainViewControllerWithMapDelegate.h"

@interface MainViewControllerWithMapView () <MainViewControllerWithMapDelegate>

    @property (nonatomic, strong) MKMapView *mapView;
    @property (nonatomic, strong) MapViewModel *viewModel;
    @property (nonatomic, strong) UIButton *button;

    - (nonnull MKMapView *)configureMapView;

@end

@implementation MainViewControllerWithMapView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewModel = [[MapViewModel alloc] init];
    self.viewModel.delegate = self;
    self.mapView = [self configureMapView];
    [self.view addSubview:self.mapView];
    [self configureButton];
}

- (void)makeFetch:(id)sender {
    [self.viewModel makeFetch];
}

- (void)configureButton {
    self.button = [[UIButton alloc] initWithFrame:CGRectZero];
    self.button.backgroundColor = UIColor.systemBlueColor;
    self.button.translatesAutoresizingMaskIntoConstraints = false;
    self.button.layer.cornerRadius = 10;
    [self.button setTitle:@"Search" forState:UIControlStateNormal];
    [self.button addTarget:self action:@selector(makeFetch:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.button];
    
    [self.button.heightAnchor constraintEqualToConstant:40].active = YES;
    [self.button.widthAnchor constraintEqualToConstant:100].active = YES;
    [self.button.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-20].active = YES;
    [self.button.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
}

- (MKMapView *)configureMapView {
    MKMapView *newMap = [[MKMapView alloc] initWithFrame:self.view.frame];
    newMap.delegate = self;
    newMap.showsUserLocation = YES;
    return newMap;
}

- (void)mainViewWithMap:(nonnull MainViewControllerWithMapView *)view addPlaceMarkToMap:(nonnull TravelPlace *)place {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mapView addAnnotation:place];
    });
}

@end
