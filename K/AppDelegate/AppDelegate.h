//
//  AppDelegate.h
//  K
//
//  Created by OUT-Volkov-AD on 10/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;

@class MainViewControllerWithMapView;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) MainViewControllerWithMapView *mainViewController;

@end

