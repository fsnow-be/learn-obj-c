//
//  TravelPlace.h
//  K
//
//  Created by OUT-Volkov-AD on 11/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <Firebase.h>

@interface TravelPlace: NSObject <MKAnnotation>

@property (nonatomic, copy)NSString *name;
@property (nonatomic, copy)NSString *about;
@property (nonatomic)int_fast16_t type;
@property (nonatomic, copy)NSString *address;

- (id)init:(FIRDocumentSnapshot *)document;

@end
