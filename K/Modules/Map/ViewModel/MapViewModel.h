//
//  NSObject+MapViewModel.h
//  K
//
//  Created by OUT-Volkov-AD on 11/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

@import UIKit;
#import "FIRNetworkFetcherDatabaseDelegate.h"
#import "MainViewControllerWithMapDelegate.h"
#import "PlaceDatabaseService.h"
#import "LocationService.h"
#import "TravelPlace.h"

NS_ASSUME_NONNULL_BEGIN

@interface MapViewModel: NSObject

@property (nonatomic, strong, nonnull) PlaceDatabaseService *service;
@property (nonatomic, strong, nonnull) LocationService *location;
@property (nonatomic, weak) id <MainViewControllerWithMapDelegate> delegate;

- (void)makeFetch;
@end

NS_ASSUME_NONNULL_END
