//
//  MainViewControllerWithMapView.h
//  K
//
//  Created by OUT-Volkov-AD on 10/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import "MapViewModel.h"
@import UIKit;
@import MapKit;

NS_ASSUME_NONNULL_BEGIN

@interface MainViewControllerWithMapView : UIViewController <MKMapViewDelegate>

@end

NS_ASSUME_NONNULL_END
