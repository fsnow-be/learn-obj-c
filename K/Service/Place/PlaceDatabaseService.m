//
//  NSObject+PlaceDatabaseService.m
//  K
//
//  Created by OUT-Volkov-AD on 11/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import "PlaceDatabaseService.h"

@interface PlaceDatabaseService ()
@end

@implementation PlaceDatabaseService

+ (id)shared {
    static PlaceDatabaseService *sharedBase = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedBase = [[self alloc] init];
    });
    return sharedBase;
}

- (id)init {
    if ((self = [super init])) {
        _defaultFirestore = [FIRFirestore firestore];
    }
    return self;
}

- (void)getCollectionOfPlacesFromDatabase {
    [[self.defaultFirestore collectionWithPath:@"places"] getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
        if (error == nil) {
            NSMutableArray *array = [NSMutableArray new];
            for (FIRDocumentSnapshot *document in snapshot.documents) {
                TravelPlace *newPlace = [[TravelPlace alloc] init:document];
                [array addObject:newPlace];
            }
            [self->_delegate networkFetcher:self didReceiverData:array];
        } else {
            [self->_delegate networkFetcher:self didFailWithError:error];
        }
    }];
}

@end
