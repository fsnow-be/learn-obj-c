//
//  FIRNetworkFetcherDatabaseDelegate.h
//  K
//
//  Created by OUT-Volkov-AD on 11/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import <Foundation/Foundation.h>
@import Firebase;

NS_ASSUME_NONNULL_BEGIN

@class PlaceDatabaseService;

@protocol FIRNetworkFetcherDatabaseDelegate

- (void)networkFetcher:(PlaceDatabaseService *)service
      didReceiverData:(NSMutableArray *)TravelPlaces;

@optional
- (void)networkFetcher:(PlaceDatabaseService *)service
      didFailWithError:(NSError *)error;

@end

NS_ASSUME_NONNULL_END
