//
//  NSObject+PlaceDatabaseService.h
//  K
//
//  Created by OUT-Volkov-AD on 11/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FIRNetworkFetcherDatabaseDelegate.h"
#import "TravelPlace.h"
@import Firebase;

NS_ASSUME_NONNULL_BEGIN

@interface PlaceDatabaseService: NSObject

@property (nonatomic, strong) FIRFirestore *defaultFirestore;
@property (nonatomic, weak) id <FIRNetworkFetcherDatabaseDelegate> delegate;

- (id)init;
+ (id)shared;
- (void)getCollectionOfPlacesFromDatabase;

@end

NS_ASSUME_NONNULL_END
