//
//  LocationService.h
//  K
//
//  Created by OUT-Volkov-AD on 12/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationService: NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;

@end
