//
//  AppDelegate.m
//  K
//
//  Created by OUT-Volkov-AD on 10/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()


@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self prepareWindows];
    [FIRApp configure];
    return YES;
}

- (void)prepareWindows {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.mainViewController = (MainViewControllerWithMapView *)[storyboard instantiateInitialViewController];
    
    UINavigationController *mainNavigationController = [[UINavigationController alloc] initWithRootViewController:(MainViewControllerWithMapView *)self.mainViewController];
    mainNavigationController.navigationBarHidden = YES;
    self.window.rootViewController = mainNavigationController;
    [self.window makeKeyAndVisible];
}

@end
