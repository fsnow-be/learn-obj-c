//
//  MainViewControllerWithMapDelegate.h
//  K
//
//  Created by OUT-Volkov-AD on 12/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class MapViewModel;
@class TravelPlace;

@protocol MainViewControllerWithMapDelegate

- (void)mainViewWithMap:(MapViewModel *)viewModel
      addPlaceMarkToMap:(TravelPlace *)place;

@end

NS_ASSUME_NONNULL_END
