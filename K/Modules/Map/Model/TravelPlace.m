//
//  TravelPlace.m
//  K
//
//  Created by OUT-Volkov-AD on 11/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import "TravelPlace.h"

@interface TravelPlace ()

-(CLLocationCoordinate2D)fromFirebasePointToCLLocation:(FIRGeoPoint *)point;

@end

@implementation TravelPlace

@synthesize coordinate;
@synthesize title;

- (NSString *) subtitle {
    return self.name;
}

- (id)init:(FIRDocumentSnapshot *)document {
    if (self == [super init]) {
        _name = document[@"name"];
        _address = document[@"address"];
        coordinate = [self fromFirebasePointToCLLocation:document[@"location"]];
    }
    return self;
}

-(CLLocationCoordinate2D)fromFirebasePointToCLLocation:(FIRGeoPoint *)point {
    return CLLocationCoordinate2DMake(point.latitude, point.longitude);
}

@end
