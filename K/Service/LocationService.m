//
//  LocationService.m
//  K
//
//  Created by OUT-Volkov-AD on 12/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationService.h"

@implementation LocationService

- (void)requestForGeolocation {
    [self.locationManager requestWhenInUseAuthorization];
}

- (void)startListningLocation {
    [self.locationManager startUpdatingLocation];
    [self.locationManager startMonitoringSignificantLocationChanges];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self startListningLocation];
        
        case kCLAuthorizationStatusNotDetermined:
            [self requestForGeolocation];
            
        case kCLAuthorizationStatusAuthorizedAlways:
            [self startListningLocation];
            
        case kCLAuthorizationStatusDenied:
            NSLog(@"kCLAuthorizationStatusDenied");
        
        case kCLAuthorizationStatusRestricted:
            NSLog(@"kCLAuthorizationStatusRestricted");
    }
}

- (id)init {
    if (self = [super init]) {
        _locationManager = [CLLocationManager new];
        _locationManager.delegate = self;
    }
    return self;
}

@end

