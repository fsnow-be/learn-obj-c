//
//  NSObject+MapViewModel.m
//  K
//
//  Created by OUT-Volkov-AD on 11/02/2020.
//  Copyright © 2020 AV. All rights reserved.
//

#import "MapViewModel.h"

@interface MapViewModel () <FIRNetworkFetcherDatabaseDelegate>
@end

@implementation MapViewModel

- (void)networkFetcher:(id)service didReceiverData:(nonnull NSMutableArray *)travelPlaces {
    for (TravelPlace *place in travelPlaces) {
        [_delegate mainViewWithMap:self addPlaceMarkToMap:place];
    }
}

- (void)makeFetch {
    [self.service getCollectionOfPlacesFromDatabase];
}

- (id)init {
    if (self = [super init]) {
        _service = [PlaceDatabaseService shared];
        _location = [LocationService new];
        _service.delegate = self;
    }
    return self;
}

@end
